package com.epam.javaacademy.stringreverse.strategy;

class NullStrategy implements Strategy {

    @Override
    public String reverse(String str) {
        throw new IllegalArgumentException();
    }
}
