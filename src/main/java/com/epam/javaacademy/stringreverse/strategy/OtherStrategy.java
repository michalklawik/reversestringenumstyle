package com.epam.javaacademy.stringreverse.strategy;

import com.epam.javaacademy.stringreverse.Reverse;

class OtherStrategy implements Strategy {

    private Reverse reverse = new Reverse();

    @Override
    public String reverse(String str) {
        return str.charAt(str.length() - 1) + reverse.reverseString(str.substring(0, str.length() - 1));
    }
}
