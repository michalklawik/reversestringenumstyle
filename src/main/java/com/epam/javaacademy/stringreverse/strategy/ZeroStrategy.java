package com.epam.javaacademy.stringreverse.strategy;

class ZeroStrategy implements Strategy {

    @Override
    public String reverse(String str) {
        return str;
    }
}
