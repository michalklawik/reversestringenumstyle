package com.epam.javaacademy.stringreverse.strategy;

class OneStrategy implements Strategy {

    @Override
    public String reverse(String str) {
        return str;
    }
}
