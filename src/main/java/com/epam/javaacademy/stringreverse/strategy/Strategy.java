package com.epam.javaacademy.stringreverse.strategy;

public interface Strategy {

    String reverse(String str);
}
