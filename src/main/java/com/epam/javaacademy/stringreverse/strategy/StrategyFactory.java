package com.epam.javaacademy.stringreverse.strategy;

import java.util.HashMap;
import java.util.Map;

public class StrategyFactory {

    private Map<Integer, Strategy> operationsMap = new HashMap<>();

    public StrategyFactory() {
        this.operationsMap.put(0, new ZeroStrategy());
        this.operationsMap.put(1, new OneStrategy());
        this.operationsMap.put(null, new NullStrategy());
    }

    public Strategy getOperation(Integer length) {
        return operationsMap.getOrDefault(length, new OtherStrategy());
    }
}
