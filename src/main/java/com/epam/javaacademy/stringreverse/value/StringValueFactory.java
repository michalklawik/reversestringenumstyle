package com.epam.javaacademy.stringreverse.value;

import java.util.HashMap;
import java.util.Map;

public class StringValueFactory {

    private Map<String, StringValue> stringToReverseMap = new HashMap<>();

    public StringValueFactory() {
        stringToReverseMap.put(null, new NullString());
    }

    public StringValue getStringValue(String str) {
        return stringToReverseMap.getOrDefault(str, new CorrectString(str));
    }
}
