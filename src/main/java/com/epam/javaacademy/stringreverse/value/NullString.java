package com.epam.javaacademy.stringreverse.value;

class NullString implements StringValue {
    @Override
    public Integer getLength() {
        return null;
    }
}
