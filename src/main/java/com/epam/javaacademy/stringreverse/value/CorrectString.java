package com.epam.javaacademy.stringreverse.value;

class CorrectString implements StringValue {

    private String stringToReverse;

    CorrectString(String stringToReverse) {
        this.stringToReverse = stringToReverse;
    }

    @Override
    public Integer getLength() {
        return stringToReverse.length();
    }
}
