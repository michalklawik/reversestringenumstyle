package com.epam.javaacademy.stringreverse.value;

public interface StringValue {

    Integer getLength();
}
