package com.epam.javaacademy.stringreverse;

import com.epam.javaacademy.stringreverse.strategy.Strategy;
import com.epam.javaacademy.stringreverse.strategy.StrategyFactory;
import com.epam.javaacademy.stringreverse.value.StringValue;
import com.epam.javaacademy.stringreverse.value.StringValueFactory;

public class Reverse {

    private StrategyFactory strategyFactory = new StrategyFactory();
    private StringValueFactory stringValueFactory = new StringValueFactory();

    public String reverseString(String str) {
        StringValue stringValue = stringValueFactory.getStringValue(str);
        Strategy strategy = strategyFactory.getOperation(stringValue.getLength());
        return strategy.reverse(str);
    }
}
