package com.epam.javaacademy.stringreverse;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReverseTest {

    @Test
    public void reverseTest() {
        String str = "PRZEMEK";
        Reverse reverse = new Reverse();
        String actual = reverse.reverseString(str);
        assertEquals("KEMEZRP", actual);
    }

    @Test
    public void emptyStringTest() {
        String str = "";
        Reverse reverse = new Reverse();
        String actual = reverse.reverseString(str);
        assertEquals("", actual);
    }

    @Test
    public void stringWithLengthOne() {
        String str = "A";
        Reverse reverse = new Reverse();
        String actual = reverse.reverseString(str);
        assertEquals("A", actual);
    }

    @Test
    public void stringWithLengthTwo() {
        String str = "AB";
        Reverse reverse = new Reverse();
        String actual = reverse.reverseString(str);
        assertEquals("BA", actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullTest() {
        new Reverse().reverseString(null);
    }
}
